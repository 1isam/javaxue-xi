package Example1;

import javax.swing.*;

public class Example01 {
    private static void CreateShowGUI(){
        JFrame jFrame = new JFrame("Java GUI Test");
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(150, 250);
        jFrame.setVisible(true);
    }
    public static void main(String[] args){
        SwingUtilities.invokeLater(Example01::CreateShowGUI);
    }
}
